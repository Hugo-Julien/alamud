# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .effect import Effect2, Effect3
from mud.events import TirerOnWithEvent, HitOnWithEvent

class TirerOnWithEffect(Effect3):
	EVENT = TirerOnWithEvent

class HitOnWithEffect(Effect3):
	EVENT = HitOnWithEvent
